postfix operator ❤︎ { }


func + (left:String, right:Test) -> Test{
	return Test(name:(left + " " + right.name))
}

func + (left:Test, right:Test) -> Test{
	return Test(name:(left.name + " " + right.name))
}

postfix func ❤︎ ( val:Test) -> Void{
	print("【TEST DEBUG】val is " + val.name)
}


class Test{
	let name:String
	init(name:String){self.name = name}
}

var a = Test(name:"hey")
var b = Test(name:"yo")
var c = Test(name:"you")
var d = Test(name:"do")


print((a + b).name)
print(("oh" + c).name)

a❤︎
b❤︎
c❤︎
d❤︎

// print((c + "name").name)
