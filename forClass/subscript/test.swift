func main(){
	let a = SubscriptTest()

	print("========");
	print(a[1]);
	print(a[2]);
	print(a[3]);
	print("========");
	print(a["hey"]);
	print(a["yo"]);
	print("========");
	print(a["hey", 5]);
	print(a["yo", 2]);
	print("========");
}

class SubscriptTest{

	subscript(val:Int)->Int{
		get{
			return val*2;
		}
	}

	subscript(val:String)->String{
		get{
			return val + "!!"
		}
	}

	subscript(val:String, times:Int)->String{
		get{
			var a = ""
			for _ in 0..<times{
				a += val
			}
			return a;
		}
	}

}


main()
