print("=============")

func test() -> ( print:()->(), name: (String)->() )  {

	var name = "Tom"
	
	return (
			{ () -> () in
				print("Hey I am \(name)")
				print("=============")
			},
			{ (newName:String) ->  Void in
				print("name changed form \(name) to \(newName)");
				name = newName;
				print("=============")
			}
		)
}

//最初のテスト
var greet = test()
greet.print()
greet.name("Jon")
greet.print()
