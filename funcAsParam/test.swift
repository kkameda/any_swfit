print("=============")

func test() -> ( print:(Double, Double)->(), method:( (Double, Double) -> Double)->() )  {

	var method:(Double, Double) -> Double = (+)
	let name = "add"
	
	return (
			{ (a:Double, b:Double) -> Void in
				print("\(a) \(name) \(b) = \(method(a,b))")
				print("=============")
			},
			{ ( newMethod:(Double, Double) -> Double) ->  Void in
				method = newMethod
				print("method changed");
			}
		)
}

//最初のテスト
var cal = test();
cal.print(20, 10);

//関数を渡す
func add(a:Double, b:Double) -> Double{
	return a + b
}
cal.method(add)
cal.print(20, 10);

//クラスメソッドを渡す（型メソッド)
class Piyo{
	class func add(a:Double, b:Double) -> Double{
		return a + b
	}
}
cal.method(Piyo.add)
cal.print(20, 10);

//インスタントメソッドも渡せる
class Piyo2{
	 func add(a:Double, b:Double) -> Double{
		return a + b
	}
}
cal.method(Piyo2().add)
cal.print(20, 10);

//直接lambdaを渡す
cal.method( { (a:Double, b:Double) -> Double in
		return a + b
		})
cal.print(20, 10);

//省略記法
cal.method { (a:Double, b:Double) -> Double in
	return a + b
}

//省略記法
cal.method() { (a:Double, b:Double) -> Double in
	return a + b
}
cal.print(20, 10);

//省略記法
cal.method({a, b in return a + b})
cal.print(20, 10);

//省略記法
cal.method({a, b in a + b})
cal.print(20, 10);

//省略記法
cal.method({ return $0 + $1})
cal.print(20, 10);

//省略記法
cal.method({$0 + $1})
cal.print(20, 10);

//省略記法
//+ is (Double, Double) -> Double
cal.method(+)
cal.print(20, 10);
