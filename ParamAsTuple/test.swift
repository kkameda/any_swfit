
print("=================");

func test(a:Int, _ b:Int){
	print(a + b);
}

test(10, 20);
//引数はタプル
let tap = (10 , 20)
test(tap);

//タプルの型
func test2With(a:Int, and b:Int){ 
	print(a + b);
}
let tap2 = (10, and:20)
test2With(tap2)
//error -> test2With(tap)

//タプルのキャスト方向性
let tap3 = (d:10, and:20)
//error -> test2With(tap3)
let tap4:(Int, and:Int) = tap3;
test2With(tap4)



//変数も実はタプル
print("=================");
var tup5 = 5;
print(tup5)
//print(tup5.0.0.0.0.0.0.0.0.0)
//swift 1.2まで使用できてたやつ
tup5 = ((((((((((((((((((((((5))))))))))))))))))))));
print(tup5)
//voidもタプル
print ( "Void is \( Void() )" )

//関数ももちろんタプルに入れることができる
func test5() -> ( add:(Double, Double)-> (Double), mul:(Double, Double)-> (Double) , minus:(Double, Double)->  (Double)){
	return (+, *, -);
}

print("=================");
let a = test5();
print(a.add(10,10));
print(a.mul(10,10));
print(a.minus(10,10));

let (_, _, b) = a;
print(b(10,10));

let d = (1, 2, tes:3);
print(d.tes);
let e:(Int, add:Int, Int) = d;
//print(e.tes);

let f:(Int, hoge:Int) = (10 , 9);
//error let g;(Int, hoge:Int) = f;
//error let g;(hoge:Int, Int) = f;


