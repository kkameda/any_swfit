
class Test {
	deinit{
		print("無事消去されました");
	}
}

var a:Test? = Test();
var b:Test? = a;
var c:Test? = a;
var d:Test? = a;

print("a = nil");
a = nil

print("b = nil");
b = nil

print("c = nil");
c = nil

print("d = nil");
d = nil
