
class Test {

	var name:String = "Tom"
	lazy var p:()->( Void -> Void ) = { //[unowned self] in
		var a:Test = self
		print("my name is \(a.name)")
		return { //[unowned a]
			() -> () in
			var b:Test = self;

			print("\(b.name) and \(a.name) is still here");	
		}
	}

	
	func doPrint()->( Void -> Void ){
		return p();
	}
	deinit{
		print("消去完了")
	}

}

var a:Test? = Test();
var b:(()->(Void -> Void))? = a!.p
var tom:(()->())? = b?()
print("a = nil")
a = nil;
tom!()
print("tom = nil")
tom = nil
