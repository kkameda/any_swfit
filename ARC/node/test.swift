//例1
class Person{
	//weak var apertment:Apertment?
	var apertment:Apertment?
	deinit{
		print("Personが消去されました")
	}
}
class Apertment{
	var person:Person?
	deinit{
		print("Apertmentが消去されました")
	}
}

var person:Person? = Person();
var apertment:Apertment? = Apertment();
person!.apertment =  apertment;
apertment!.person = person;

print("person = nil")
person = nil
print("apertment = nil")
apertment = nil


print("==========例2==============")
//例2
class Node{

	let name:String
	//weak var friend:Node?{
	 var friend:Node?{
		didSet{
			if ((friend !== nil) && (friend!.friend !== self)){
				print("friend \(friend!.name)がセットされました")
				friend!.friend = self
			}
		}
	}
	init(_ name:String){
		print("\(name)初期化されました")
		self.name = name
	}
	deinit{
		print("\(name)無事消去")
	}
}


var a:Node? = Node("node1");
var b:Node? = Node("node2");
a!.friend = b;


print("a = nil")
a = nil
print("b = nil")
b = nil


